# Adding ext methods to standard classes
# .frozen_string_literal: true

# Add method to_b for convert value to dec bytes
class Integer
  def to_b(dec)
    res = ''
    val = dec
    while val.negative?
      res += (val << 8).to_s
      val <<= 8
    end
    res.ljust(dec, 0.to_s)
    # p "#@res #{:dec}"
  end
  alias to_bin to_b
end

# Add method to_b for convert value to dec bytes
class String
  def to_b(dec)
    self[0..dec - 1].ljust(dec, "\x0").to_b(dec)
  end
end
