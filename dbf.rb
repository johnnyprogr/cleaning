require "./acquire_dbf"

DBF_FLD_TYPES = {:boolean  => 'L',
            :date     => 'D',
            :float    => 'F',
            :number   => 'N',
            :string   => 'C',
            :memo     => 'M'}
DBF_CODE_PAGES = { 87.chr  => "ANSI",
                  37.chr  => "OEM",
                  0.chr   => "NONE"}

class DBFField
  attr_accessor
    :name      # 0-10 - ��� ���� (����������� ������)
    :type      # 11           - ��� ����
    :offset    # 12-15        - ������������ ���� ������ ������
    :size      # 16           - ������ ���� (� ������)
    :precision # 17           - ���������� ������ ����� �������
    :dummy     # 18-31 - ���������������

  def initialize(name, type, size, precision)
    @name       = name[0..10].ljust(11, " ") #�������
    @type       = type
    @offset     = 0
    @size       = size
    @precision  = precision
    @dummy      = "\x0" * 14
  end

  def size
    32
  end

  def set_name(val)
    @name = val[0..10].ljust(11, " ") #������� ������
  end
  
  def to_bin
    @name + DBF_FLD_TYPES[@type] + @offset.to_b(4) + @size.to_b(1) + @precision.to_b(1) + @dummy
  end
end

class DBFFields < Array
  def initialize
    self = Array.new(DBFField)
  end

  def to_bin
    self.inject([]){|r,el|r << el.to_bin}.join
  end
end

class DBFHeader
  attr_accessor
    :version      # 0     - ������ dbf-�����
    :year         # 1     - ��� ���������� ����������
    :month        # 2     - ����� ���������� ����������
    :day          # 3     - ���� ���������� ����������
    :record_count # 4-7   - ���������� ������� � ����� ������� ���������
    :header_size  # 8-9   - ������ ��������� (� ������)
    :record_size  # 10-11 - ������ ������ (� ������)
    :dummy1       # 12-13 - ���������������
    :transaction  # 14    - ���� ������������� ���������� dBASE IV
    :encrypt      # 15    - ���� ��������������� ������� dBASE IV
    :multi_use    # 16-19 - ��������������� ��� ��������
    :last_user_id # 20-23   ������������� - dBASE IV, dBASE V
    :dummy2       # 24-27 - ���������������
    :mdx_flag     # 28    - ���� ������� MDX-�����
    :language     # 29    - ID �������� ����� (������� ��������)
    :dummy3       # 30-31 - ���������������
    :fields       # �������� �����
    
    def initialize
      @version      = "\x0"
      @year         = "\x0"
      @month        = "\x0"
      @day          = "\x0"
      @record_count = "\x0" * 4
      @header_size  = "\x0" * 2
      @record_size  = "\x0" * 2
      @dummy1       = "\x0" * 2
      @transaction  = "\x0"
      @encrypt      = "\x0"
      @multi_use    = "\x0"
      @last_user_id = "\x0" * 4
      @dummy2       = "\x0" * 4
      @mdx_flag     = "\x0"
      @language     = "\x0"
      @dummy3       = "\x0" * 2
      @fields       = DBFFields.new
    end

    def size
      @fields.inject(32) { |r, i|r + i.size}
    end

    def to_bin
      @version + @year + @month + @day 
      + @record_count.to_i.to_bin(4)
      + (32 + @fields.size * 32).to_bin(2) 
      + @dummy1
      + @transaction 
      + @encrypt + @multi_use + @last_user_id + @dummy2 + @mdx_flag + @language + @dummy3
      + @fields.to_bin
    end

    def add_field(name, type, size, precision)
      @fields << DBFField.new(name, type, size, precision)
    end
end

class DBF
  attr :header
  attr_accessor :table_name

  def initialize
    @header = DBFHeader.new
    @table_name = ""
  end

end